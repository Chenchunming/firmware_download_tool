; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDownLoadDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "DownLoad.h"

ClassCount=3
Class1=CDownLoadApp
Class2=CDownLoadDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_DOWNLOAD_DIALOG

[CLS:CDownLoadApp]
Type=0
HeaderFile=DownLoad.h
ImplementationFile=DownLoad.cpp
Filter=N

[CLS:CDownLoadDlg]
Type=0
HeaderFile=DownLoadDlg.h
ImplementationFile=DownLoadDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CDownLoadDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=DownLoadDlg.h
ImplementationFile=DownLoadDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_DOWNLOAD_DIALOG]
Type=1
Class=CDownLoadDlg
ControlCount=10
Control1=IDC_STATIC,static,1342308352
Control2=IDC_EDIT_FILEINFO,edit,1350633600
Control3=IDC_BUTTON_CHOICE,button,1342242816
Control4=IDC_EDIT_NOTE,edit,1350633600
Control5=IDC_STATIC,static,1342308352
Control6=IDC_BUTTON_DOWN,button,1342242816
Control7=IDC_BTN_CANCEL,button,1342242816
Control8=IDC_EDIT_PROGRESS,edit,1350633600
Control9=IDC_BUTTON_RECONN,button,1342242816
Control10=IDC_COMBO1,combobox,1344340226

