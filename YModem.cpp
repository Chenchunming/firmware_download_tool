// YModem.cpp: implementation of the YModem class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "YModem.h"
#include "DownLoad.h"
#include "DownLoadDlg.h"
#include <Afxmt.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
extern CCriticalSection m_criticalSection;
extern CDownLoadDlg DownLoaddlg;
typedef  unsigned char  uint8;	
typedef  unsigned short  uint16;	
typedef  UINT32  uint32;
extern BOOL ReadWait;
extern BOOL WriteWait;	

#define YMODEM_MAX_PATH       				            100					/* 文件名最大长度														*/
#define MAX_FILE_DATA_NUMS				1024 * 1024			/* 文件数据最大长度,注意最大文件长度不要超过1024*6,要按实际RAM来决定	*/
#define FRAME_DATA_MAX_NUM				256 * 3				/* 一帧数据的最大数据长度 												*/
	 /*
	 *	这些宏定义请参阅<<最新网络通讯协议手册>>
	 */
#define MODEM_NUL  						0X00				/* 空值 																*/
#define MODEM_SOH  						0x01        		    /* 数据块起始字符 														*/
#define MODEM_STX  						0x02				    /* start of text 文本开始 												*/
#define MODEM_EOT  						0x04				    /* End of transmission 传输结束 										*/
#define MODEM_ACK  						0x06				    /* Acknowledge 确认或肯定回答 											*/
#define MODEM_XON_DC1					0X11				    /* XON 																	*/
#define MODEM_XOFF_CD3  				0x13				    /* XOFF 																*/
#define MODEM_NAK						    0X15				    /* Negative acknowledge 非确认或否定回答 								*/
#define MODEM_CAN						    0X18				    /* Cancel 清除 															*/
#define MODEM_SUB  						0x1A				    /* Substitute , End Of File 调换,文件结束 								*/
#define MODEM_SP						        0X20				/* Space 空格 															*/
#define MODEM_C    						    0x43				    /* Modem协议使用CRC校验 												*/
#define MODEM_CHECKSUM				0XFA				/* Modem协议使用校验和来校验											*/

	 /*
	 *   这里定义的宏指明了协议解析中正在进行的步骤,这里将ARM作为接收端
	 *	
	 */
#define RECEIVE_CHECK_TYPE				            0X00			/* 当前协议将要接收校验类型 											*/
#define RECEIVE_MODEM_SYNCHR_HEAD		0X01						/* 当前协议将要接收数据包头												*/
#define RECEIVE_BLOCK_NUMBER			            0X02			/* 接收块号 															*/
#define RECEIVE_BLOCK_NUMBER_PATCH		0X03						/* 接收块号补码															*/
#define RECEIVE_FILE_INFO				                0X04			/* 接收文件信息,如接收文件名,文件大小 									*/
#define RECEIVE_FILE_DATA				                0X05			/* 接收文件数据															*/
#define RECEIVE_FRAME_CHECK				        0X06			/* 接收一帧数据的校验													*/
#define RECEIVE_IDLE					                    0X07			/* 接收处于空闲状态,表明此时未收到任何命令 								*/
#define RECEIVE_TIMEOUT					            0X08			/* 接收数据超时 														*/
#define RECEIVE_FRAME_DATA				            0X09			/* 接收帧数据															*/
#define RECEIVE_CRC1					                    0X0A			/* 接收CRC1校验 														*/
#define RECEIVE_CRC2					                    0X0B			/* 接收CRC2校验 														*/
#define RECEIVE_CHECKSUM				                0X0C			/* 接收校验和校难			 											*/
#define RECEIVED_ONE_FRMAE				        0X0D

#define RECEIVED_EOT					                    0X0E			/* 收到一个文件结束标志 												*/
#define RECEIVED_FILE_INFO_NUL			        0X0F			/* 收到NUl的信息头 														*/
#define RECEIVED_FILE					                    0X10			    /* 确认收到一个文件 													*/
#define MODEM_IDLE_STEP					            0X11			    /* 这里指定通讯步骤为空模式 */
#define RECEIVING_EOF					                0X12			    /*  */
 /********************************
	 宏描述:这里指明了YModem缓冲区中文件的状态
*********************************/
#define MODEM_NO_FILE							                             0X00	        /* 缓冲区里没有数据 											*/
#define MODEM_RECEIVED_ONE_FILE					                  0X01	        /* YModem收到了一个文件 										*/
#define MODEM_RECEIVED_FILE_WAIT_NEXT_FILE 		      0X02	         /* YModem收到了一个文件,但又收到发送下一个文件的命令帧 			*/
#define WIRELESS_RECEIVED_ONE_FILE				                  0X03	        /* 无线收到一个文件,数据存储在YModem中,并且后续没有文件再次发送	*/
#define WIRELESS_RECEIVED_FILE_WAIT_NEXT_FILE       0X04	        /* 无线收到一个文件,并且收到下发下一个文件的命令 				*/


#define MAX_TIMEOUT_TIMES						0XFF			/* 无线发送一个文件最大失败次数 								*/

	 /*
	 * 	这里定义的宏指明了协议在发送数据正在进行的步骤,这里将ARM作为发送端
	 */
#define SEND_SOH						                             0X80
#define SEND_SHX						                             0X81						
#define SEND_BLOCK						                         0X82
#define SEND_BLOCK_PATCH				                     0X83
#define SEND_DATA						                         0X84
#define SEND_CHECK						                         0X85
#define SEND_CRC1						                             0X86
#define SEND_CRC2						                         0X87
#define SEND_NEXT_FRAME_DATA			             0X88
#define SEND_FILE_INFO					                     0X89
#define SEND_FILE_INFO_ACK				                 0X8A
#define SEND_FILE_INFO_NUL				                 0X8B
#define SEND_FILE_INFO_NUL_BEGIN	             0X8C
#define SEND_FILE_INFO_NUL_ACK			             0X8D
#define SEND_FILE_DATA_CHECK_MODE	             0X8E
#define SEND_FILE_DATA					                     0X8F
#define SEND_END_OF_FILE				                     0X91
#define RECEIVE_SENDED_FRAME_ACK	             0X91			/* 接收发送一帧数据后的回复 */
#define SEND_MODE_RECEIVING_BEGINSEND	 0X92			/* 发送模式下接收数据发送请求,即接收MODEM_C */


#define MODEM_RECEIVE					0X00			                    /* 当前MODEM口正作为接收端													*/
#define MODEM_SEND						0X01			                        /* 当前MODEM口正作为发送端 													*/
#define MODEM_IDLE						    0X02			                    /* 当前MODEM口处于空闲模式，此时收到字符"C"时要发送取消,且立即转为收模式 	*/

#define SEND_FILE_DATA_WAITFOR_ACK		0X00
#define SEND_FILE_DATA_SEND_NEXT_FRAME	0X01

#define DATA_BLOCK_SIZE_1024			1024
#define DATA_BLOCK_SIZE_128BYTE			128

typedef struct __Modem_struct{
	CHAR 	FileName[ YMODEM_MAX_PATH ] 				; 	               /* 文件名 																*/
	CHAR	Next_FileName[ YMODEM_MAX_PATH ]			;																
	CHAR 	FileDataBuf[ MAX_FILE_DATA_NUMS ]	; 	   /* 文件数据缓冲区 														*/
	CHAR	FrameData[ FRAME_DATA_MAX_NUM ]		;
	DWORD  currentUsingPoint					;
	DWORD  currentComDataPoint					;
	
	CHAR 	checkMode							; 	/* 当前校验模式															*/
	CHAR	CRC1 , CRC2							;	 /* CRC校验																*/
	CHAR	checkSum							;	     /* 校难和方式的校验 													*/
	CHAR	Block_Number_Patch					;/* 块号的补码 															*/
	uint32 	ValidatDataLength					;	 /* 当前缓冲区内有效数据长度,也指明当前数据接收缓冲区的指针 				*/
	uint32 	ReceivingFileLength   				;/* 当前正在接收的文件的长度				 								*/
	uint32	SendingFileDataPoint				;	/* 正在发送的文件数据的位置 											*/
	uint8	SendFileDataStatus					;
	
	uint8  	CurrentDataBlock					;	/* 当前正在接收的数据块													*/
	
	uint8  	NextDataBlock						;	/* 下一个数据块 														*/
	uint8  	CommunityStep						;	/* 当前通讯中正在进行的步骤,具体指当前是接收文件头,文件数据文件尾等		*/
	uint8	OneBlockCommunityStep			;	/* 传输一块数据时的通讯步骤 											*/
	uint8  	CommunityModel					;	/* 当前通讯状态,是接收还是发送											*/
	uint32	DataBlockSize						;	/* 指明当前数据包的大小 												*/
	uint8 	DataReady							;	/* YModem接收到数据, 												*/
	uint32 	frameDataLength						;
	uint8	TimeOutFlag							;	    /* 超时标记 															*/
	uint8	TimeOutTimes						;	    /* 超时次数,注意如果超时次数超过10次则应该取消传输 						*/
	UCHAR   FileStatus							;
}XYModem_struct; 
XYModem_struct   XY_Modem ;
typedef struct _FLD_{
	CHAR FileName[ 10 ] 	;	/* 指示文件名 				*/
	uint32 size 			; 	        /* 指示下一步所传的文件大小 */
	uint32 record 			;	   /* 指示记录的个数 			*/
	uint32 *fields			; 	   /* 指示记录中域的大小		*/
	uint32 fieldNum			;  /* 指示域的个数 			*/
	uint8 currentDataValid 	;  /* 指示FLD数据是否有效 		*/
	uint32 oneLineLength	;	   /* 指示一行有多少字节		*/
}FLD_File ;

BOOL ReceFlag = FALSE;                   /*接收字符标志*/   

YModem::YModem(void)
{
	postion = 0;
}

YModem::~YModem(void)
{
}
/*
*********************************************************************************************************
** 函数名称 Init_ModemStruct()
** 函数功能 ：初始化Modem口.
** 入口参数 ：无
** 出口参数 ：接收到的数据
*********************************************************************************************************
*/
void YModem::Init_Modem( uint8 Modem_Mode ,DWORD UserParam ){
	XY_Modem.CommunityModel 		= Modem_Mode 			; 	    
	XY_Modem.ValidatDataLength 		= 0 						; 
	XY_Modem.CurrentDataBlock  		= 0 						;
	XY_Modem.ReceivingFileLength 	= 0 						; 
	XY_Modem.NextDataBlock			= 0 						; 
	XY_Modem.OneBlockCommunityStep	= RECEIVE_MODEM_SYNCHR_HEAD ; 
	XY_Modem.DataBlockSize			= DATA_BLOCK_SIZE_128BYTE 	;
	XY_Modem.CommunityStep			= RECEIVE_FILE_INFO			;
	XY_Modem.DataReady				= FALSE 					;
	XY_Modem.checkMode				= MODEM_C					;
	XY_Modem.currentUsingPoint		= 0 						; 
	XY_Modem.currentComDataPoint	= 0 						;
	XY_Modem.FileStatus 			= MODEM_NO_FILE 			;
    m_UserParam=UserParam;          /* 传递用户参数 */
  
	if( XY_Modem.CommunityModel == MODEM_RECEIVE ){
		XY_Modem.TimeOutFlag = FALSE 	; 
		XY_Modem.TimeOutTimes = 0 		; 
	
	}else if( XY_Modem.CommunityModel == MODEM_IDLE ){
		XY_Modem.OneBlockCommunityStep	= MODEM_IDLE_STEP ;

	}else if( XY_Modem.CommunityModel == MODEM_SEND ){
		XY_Modem.CommunityStep = SEND_FILE_INFO ;
	}

	if( XY_Modem.CommunityModel != MODEM_IDLE ){
		XY_Modem.TimeOutFlag = FALSE 	; 
		XY_Modem.TimeOutTimes = 0 		; 
		
	}
}

/*
*********************************************************************************************************
** 函数名称 : ModemSend_ProcessCOM_Data( uint8 data )
** 函数功能 : 发送模式下处理串口数据
** 入口参数 : 无
** 出口参数 : 接收到的数据
*********************************************************************************************************
*/
void YModem::ModemSend_ProcessCOM_Data( uint8 data ){
	if( XY_Modem.CommunityModel != MODEM_SEND ) return ;																																	/* 非发送模式立即返回 */
	if( XY_Modem.CommunityStep == SEND_FILE_INFO && data == MODEM_C ){																									 /* 得到发送数据请求 */
		DelayNS( 1 ) ; 
        SendFileInfo( SEND_FILE_INFO ) ;
	   XY_Modem.CommunityStep = SEND_FILE_INFO_ACK ;
	}else if( XY_Modem.CommunityStep == SEND_FILE_INFO_ACK && data == (MODEM_ACK) ){
		XY_Modem.CommunityStep = SEND_FILE_DATA_CHECK_MODE ;
	}else if( XY_Modem.CommunityStep == SEND_FILE_DATA_CHECK_MODE && data == MODEM_C ){
		XY_Modem.CommunityStep = SEND_FILE_DATA ;
		XY_Modem.SendFileDataStatus = SEND_FILE_DATA_SEND_NEXT_FRAME ;   
         SendOneFrame_FileData( ) ;
		 DownLoaddlg.m_editNote.SetWindowText("正在升级程序，请等待...");
	}else if( XY_Modem.CommunityStep == SEND_FILE_DATA && ( data == (MODEM_ACK)|| data == (MODEM_NAK) ) ){																																																										/* 此时代表发送了一帧数据,并且该帧数据收到了回复 */
		if( data == (MODEM_ACK)){																																														/* 得到回复, 						*/
			XY_Modem.SendingFileDataPoint += XY_Modem.DataBlockSize ;
			XY_Modem.CurrentDataBlock ++ ;
            SendOneFrame_FileData( ) ;
		}
		if( XY_Modem.SendingFileDataPoint >= XY_Modem.ValidatDataLength ){		                                                                               /* 发送完毕,则发送结束标志 			*/
			DelayNS( 1 ) ; 
			Modem_SendByte( MODEM_EOT ) ;
			XY_Modem.CommunityStep = SEND_END_OF_FILE ;
			return ; 
		}
		XY_Modem.SendFileDataStatus = SEND_FILE_DATA_SEND_NEXT_FRAME ;
	}
	else if( XY_Modem.CommunityStep == SEND_END_OF_FILE && ( data ==(MODEM_ACK) || data == MODEM_NAK) ){														/* 发送文件结束标志,并且收到了ACK 	*/
		if( data == MODEM_NAK ){																																							 /* 如果获得了非肯定回答,则应再次回复EOT */
			Modem_SendByte( MODEM_EOT ) ;
			return ; 
		}
		XY_Modem.CommunityStep = SEND_FILE_INFO_NUL_BEGIN ;
		if( XY_Modem.FileStatus == WIRELESS_RECEIVED_FILE_WAIT_NEXT_FILE ){
			XY_Modem.CommunityStep = SEND_FILE_INFO 	;
			XY_Modem.CommunityModel 	= MODEM_IDLE ;		            																						  /* 此时已经禁止接收YModem数据 		*/	
		}
	}
	else if( XY_Modem.CommunityStep == SEND_FILE_INFO_NUL_BEGIN && ( data == MODEM_ACK || data == MODEM_C || data == MODEM_CHECKSUM ) ){
		if( data == MODEM_ACK ){																																						  /* 如果对方设备回复正确了,关闭程序 	*/
		return ; 
		}
		DelayNS( 1 ) ; 
		SendFileInfo( SEND_FILE_INFO_NUL ) ;
		Recvchar=FALSE;	
	}
}

/*
*********************************************************************************************************
** 函数名称 Modem_Request( void )
** 函数功能 ：waitting for the first "C"
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
void YModem::Modem_Request( void ){
	if(ReceFlag == TRUE)
	{ 	     					
			ReceFlag = FALSE;	 
			if( XY_Modem.CommunityModel == MODEM_SEND ){
					ModemSend_ProcessCOM_Data( XY_Modem.FrameData[0]) ;
			}else{																																																					/* MODEM处于空闲模式, */
				if( XY_Modem.CommunityModel == MODEM_IDLE && XY_Modem.FrameData[ 0] == MODEM_C ){									/* 在这里认为模块被唤醒 */
					return ; 
				}
			}
	}
	if( XY_Modem.TimeOutFlag == TRUE ){																																					 /* 某一步超时了 			*/
		XY_Modem.TimeOutFlag = FALSE ;																																					  /* 继续等待下一个超时		*/
		if( XY_Modem.CommunityModel == MODEM_RECEIVE ){																													/* 如果处于发送状态 */
		      Modem_SendByte( MODEM_C ) ;
		}
	}
	if( XY_Modem.TimeOutTimes >= MAX_TIMEOUT_TIMES ){																														 /* 超时次数超过10次 		*/
		XY_Modem.TimeOutTimes = 0 ; 																																																				 /* 超时次数达到最大次数的话就应该睡眠了,这里作超时后的处理 */
	}
}

/*
*********************************************************************************************************
** 函数名称   void Modem_SendByte( uint8 data )
** 函数功能 ：向MODEM口送一帧数据,主要功能是将串口进行抽象.
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
void YModem::Modem_SendByte( uint8 data ){
	BYTE psendbuf[1] ;
	psendbuf[0]=data;
	DownLoaddlg.Senddata( psendbuf,1 ) ; 
}    

/*  
*********************************************************************************************************
** 函数名称 : SendFile( CHAR * FileName , uint32 FileSize )
** 函数功能 ：配置发送文件函数,调用该函数后,系统进入发送模式.
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
void YModem::SendFile( CHAR * FileName , DWORD FileSize ){
	XY_Modem.CommunityModel 		= MODEM_SEND 				; 		/* 默认为发送模式 */
	XY_Modem.ValidatDataLength 		= FileSize 					; 
	XY_Modem.CurrentDataBlock  		= 1 						;
	XY_Modem.SendingFileDataPoint 	= 0 						; 

	XY_Modem.NextDataBlock			= 1 						; 
	XY_Modem.OneBlockCommunityStep	= RECEIVE_MODEM_SYNCHR_HEAD ; 
	XY_Modem.DataBlockSize			= DATA_BLOCK_SIZE_128BYTE 	;
	XY_Modem.CommunityStep			= SEND_FILE_INFO			;
	XY_Modem.DataReady				= FALSE 					;
	XY_Modem.checkMode				= MODEM_C					;
	strcpy( XY_Modem.FileName , FileName ) 						; 
	XY_Modem.ValidatDataLength		= FileSize 					;
}
/*
*********************************************************************************************************
** 函数名称 : SendOneFrame( void )
** 函数功能 ：发送一帧文件的数据
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
void YModem::SendOneFrame_FileData( void ){
	int j=0;	
	int lestnum=0;
	uint32 i ; 
	uint16 tempCRC = 0  ;
	CString str;
	uint8  tempCheckSum = 0 ; 
	BYTE  SENDBUF[1024]={0};
	BYTE  LestBuf[1024]={0};
																																													/* 先决定数据块的大小 ,是1K还是128字节,即将要发送的数据大于或等于1K,则使用1K的数据块发送 */
	if( ( XY_Modem.ValidatDataLength - XY_Modem.SendingFileDataPoint ) / DATA_BLOCK_SIZE_128BYTE >= 1 ){
		XY_Modem.DataBlockSize			= DATA_BLOCK_SIZE_1024 	;
		Modem_SendByte( MODEM_STX ) ; 
	}else{
		Modem_SendByte( MODEM_SOH ) ; 
		XY_Modem.DataBlockSize			= DATA_BLOCK_SIZE_128BYTE 	;
	}
	Modem_SendByte( XY_Modem.CurrentDataBlock ) ;
	Modem_SendByte( ~XY_Modem.CurrentDataBlock ) ;
                                                                                                                                                                           /* 从以下开始发送真正的数据 */
	for( i = 0 ; i < XY_Modem.DataBlockSize ; i ++ ){
		if( XY_Modem.SendingFileDataPoint + i >= XY_Modem.ValidatDataLength )
		{
			if (j>0&&j<1024)
			{
				DownLoaddlg.Senddata(SENDBUF,j); 
				Sleep(10);
			}
	     	break ; 	
		}
		SENDBUF[j]=(BYTE) XY_Modem.FileDataBuf[ XY_Modem.SendingFileDataPoint + i ];																																		/* 如果要发送的文件数据发完了,则退出 	*/	  
		j++;
		if(j%1024==0) 
		{
			DownLoaddlg.Senddata(SENDBUF,1024) ; 
			Sleep(10);
			j=0;
		}																                                          
	   tempCRC = CalCRC16(  XY_Modem.FileDataBuf[ XY_Modem.SendingFileDataPoint + i ] , tempCRC ) ; 
	}
	for( ; i < XY_Modem.DataBlockSize ; i ++ )                                                      /* 如果最后发的文件数据不够一帧,则发送MODEM_SUB(0x1A) */
	{
	    Modem_SendByte( MODEM_SUB ) ;		
       tempCRC = CalCRC16( MODEM_SUB , tempCRC ) ; 
	   tempCheckSum += MODEM_SUB ;
	}
																								/* 发送校验 	*/
	if( XY_Modem.checkMode == MODEM_C ){
		Modem_SendByte( tempCRC >> 8 ) ; 
		Modem_SendByte( tempCRC ) ; 
	}else{
		Modem_SendByte( tempCheckSum); 
	}
	postion = (int)(((float)XY_Modem.SendingFileDataPoint / XY_Modem.ValidatDataLength)*100);
	str.Format("%d %%", postion);
	DownLoaddlg.m_editProgress.SetWindowText(str);
}

/*
*********************************************************************************************************
** 函数名称 :    SendFileInfo( void )
** 函数功能 ：发送一帧数据
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
void YModem::SendFileInfo( uint8 FileInfoType ){
	uint32 i = 0 , j ;
	CString STR=_T("");
	CHAR tempStr[10] ;
	uint16 tempCRC = 0  ;
	UCHAR tempChksum = 0 ; 

	Modem_SendByte( MODEM_SOH ) ;
	Modem_SendByte( MODEM_NUL ) ; 						                                     /* 块号 		*/
	Modem_SendByte( 0xff ) ; 							                                            /* 块号补码	*/
	if( FileInfoType == SEND_FILE_INFO ){
		while( XY_Modem.FileName[ i ] != '\0' ){		                                        /* 发送文件名*/
			Modem_SendByte( XY_Modem.FileName[ i ] ) ; 
			tempCRC = CalCRC16( XY_Modem.FileName[ i ] , tempCRC ) ; 
			tempChksum += XY_Modem.FileName[ i ] ;
			i++ ; 
		}
		                                                                                                          /* 发送文件名结束字符'\0' */
		Modem_SendByte( MODEM_NUL ) ;
		tempCRC = CalCRC16( MODEM_NUL , tempCRC ) ; 
		tempChksum += MODEM_NUL ;
		i ++ ;
	                                                                                                            /* 发送文件大小 */
		sprintf( tempStr , "%d" , XY_Modem.ValidatDataLength ) ; 
		j = 0 ; 
		while( tempStr[ j ] != '\0' ){					                                             /* 发送文件大小 */
			Modem_SendByte( tempStr[ j ] ) ; 
			tempCRC = CalCRC16( tempStr[ j ] , tempCRC ) ; 
			tempChksum += tempStr[ j ] ;
			i++ ; 
			j++ ; 
		}
	}else{
		XY_Modem.DataBlockSize = 128 ; 
	}
	                                                                                                              /* 发送其他部分,其余部分发送 NUL */
	for( ; i < XY_Modem.DataBlockSize ; i ++ ){
		Modem_SendByte( MODEM_NUL ) ;
		tempCRC = CalCRC16( MODEM_NUL , tempCRC ) ; 
		tempChksum += MODEM_NUL ;
	}
	                                                                                                             /* 发送校验部分 */
	if( XY_Modem.checkMode == MODEM_CHECKSUM ){
		Modem_SendByte( tempChksum ) ; 					                                    /* 发送该字节为校验 	*/
	}else if( XY_Modem.checkMode == MODEM_C ){
		Modem_SendByte( (tempCRC >>8) && 0xFF) ;
		Modem_SendByte( tempCRC && 0xFF ) ;
	}
}

/*
*********************************************************************************************************
** 函数名称 : Modem_Cancel( void )
** 函数功能 ：取消命令
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
void YModem::Modem_Cancel( void ){								                     
	Modem_SendByte( MODEM_CAN ) ;
	Modem_SendByte( MODEM_CAN ) ;
	Modem_SendByte( MODEM_CAN ) ;
	Modem_SendByte( MODEM_CAN ) ;
	Modem_SendByte( MODEM_CAN ) ;
}
/*
*********************************************************************************************************
** 函数名称 : DelayNS( int delaytime )
** 函数功能 ：纳秒级延时
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
void YModem::DelayNS( int delaytime )
{  
	  delaytime=delaytime*5;
     for(int i=0;i<delaytime;i++)
	 {
	 }
}
/*
*********************************************************************************************************
** 函数名称 : CalCRC16(uint8 crcdata, uint16 crc)
** 函数功能 ：CRC16校验 采用CRC-16/MODBUS    X16+X15+X2+1    
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
int YModem::CalCRC16(uint8 crcdata, uint16 crc)                             
{  
	int c, j;
	if (crc == 0)
	{
		crc = 0xffff;
	}
	c = crcdata & 0x00FF;
	crc ^= c;
	for (j = 0; j < 8; j++) 
	{
		if ((crc & 0x0001) != 0) 
		{
				crc >>= 1;
				crc ^= 0xA001;
		} else
				crc >>= 1;
		}
      return (crc);
}
/*
*********************************************************************************************************
** 函数名称 : Receivedata(UINT8 RECVchar)
** 函数功能 ：串口读入的数据 
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
void  YModem::Receivedata(uint8 RECVchar)
{
	XY_Modem.FrameData[0] =RECVchar;
	ReceFlag=TRUE;
}
/*
*********************************************************************************************************
** 函数名称 : Receivebuf(char* fileBuff ,DWORD32  buflength)
** 函数功能 ：发送文件的内容
** 入口参数 ：无
** 出口参数 ：无
*********************************************************************************************************
*/
 void  YModem::Receivebuf(char* fileBuff ,DWORD32  buflength)
 {
	 for (DWORD32 i=0;i<buflength;i++)
	 {
		 XY_Modem.FileDataBuf[i]=fileBuff[i];
	 }
	 ReceFlag= FALSE;
 }

