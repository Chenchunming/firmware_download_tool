// YModem.h: interface for the YModem class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_YMODEM_H__AB465515_A447_46FD_B4F8_7FADB93747D2__INCLUDED_)
#define AFX_YMODEM_H__AB465515_A447_46FD_B4F8_7FADB93747D2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "YModem.h"
#include <Afxmt.h>
class YModem
{
	
public:
    HANDLE  hsendthread;
	BOOL   Recvchar;
	
	typedef  unsigned char  uint8;	
	typedef  unsigned short uint16;	
	typedef  unsigned int  uint32;	
	
	YModem(void);
	~YModem(void);
    void SendFile( CHAR * FileName , DWORD FileSize );
	void  ModemSend_ProcessCOM_Data( uint8 data );
	void Init_Modem( uint8 Modem_Mode,DWORD UserParam ) ; 
	void Modem_Request( void ) ;
	void Modem_SendByte( uint8 data ) ;
	void Modem_Cancel( void ) ;
	void SendFile( CHAR * FileName , uint32 FileSize ) ;
	void SendOneFrame_FileData( void ) ;
	void SendFileInfo( uint8 FileInfoType ) ;
	void Modem_Send_Request( void ) ;
	void DelayNS(int delaytime);
    int CalCRC16(uint8 crcdata, uint16 crc);
    void  Receivedata(uint8 RECVchar);
    void Receivebuf(char* fileBuff ,DWORD32  buflength);
	DWORD m_UserParam;
    int postion;
};
#endif // !defined(AFX_YMODEM_H__AB465515_A447_46FD_B4F8_7FADB93747D2__INCLUDED_)
