// DownLoadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DownLoad.h"
#include "DownLoadDlg.h"
#include "YModem.h"
#include <Afxmt.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define		MULTICHAR_RECV                 1024
#define     MODEM_SEND		               0X01			     // 当前MODEM口正作为发送端
#define		YModem_ACK                     0X66
#define		ID_ACK                         0X67
YModem m_YModem;
CCriticalSection m_criticalSection;
BOOL ReadWait = TRUE;
BOOL WriteWait = TRUE;
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDownLoadDlg dialog

CDownLoadDlg::CDownLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDownLoadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDownLoadDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDownLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDownLoadDlg)
	DDX_Control(pDX, IDC_EDIT_PROGRESS, m_editProgress);
	DDX_Control(pDX, IDC_EDIT_NOTE, m_editNote);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDownLoadDlg, CDialog)
	//{{AFX_MSG_MAP(CDownLoadDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_DOWN, OnButtonDown)
	ON_BN_CLICKED(IDC_BUTTON_CHOICE, OnButtonChoice)
	ON_BN_CLICKED(IDC_BTN_CANCEL, OnBtnCancel)
	ON_BN_CLICKED(IDC_BUTTON_RECONN, OnButtonReconn)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDownLoadDlg message handlers
/*******************************************************************************************
函数名称: CCeSerial::CommRecvTread
描    述: 串口接收线程
输入参数: LPVOID lparam: 线程参数,创建线程时传入
输出参数: 无
返    回: 0: 线程退出, 返回值没特殊含义
********************************************************************************************/
DWORD CommRecvTread(LPVOID lparam)
{
	CDownLoadDlg *pSerialPort = (CDownLoadDlg *)lparam;
	DWORD dwLength = 0;
	DWORD dwError = 0;
	DWORD dwByte = 0;
	BYTE recvBuf[1] = {0};
	COMSTAT comStat;
	OVERLAPPED olRead;
	
	memset(&olRead,0,sizeof(olRead)); 
	olRead.hEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
	
	if (pSerialPort->m_hCom)
	{
		m_criticalSection.Lock();
		ClearCommError(pSerialPort->m_hCom, &dwError, &comStat);
		PurgeComm(pSerialPort->m_hCom, PURGE_RXCLEAR | PURGE_TXCLEAR | PURGE_RXABORT | PURGE_TXABORT);
		m_criticalSection.Unlock();
	}
	ZeroMemory(recvBuf, 1);
	while(ReadWait)
	{																
		if (pSerialPort->m_hCom != INVALID_HANDLE_VALUE)
		{															                                      
			m_criticalSection.Lock();
			BOOL fReadState = ReadFile(pSerialPort->m_hCom, recvBuf, 1, &dwLength, &olRead);
			m_criticalSection.Unlock();
			if(GetOverlappedResult(pSerialPort->m_hCom, &olRead, &dwLength, TRUE) == FALSE)
			{
				continue;
			}else{
				if(dwLength != 0)					// 接收成功调用回调函数 
				{
					if (recvBuf[0] == 'F')
					{
						pSerialPort->m_editNote.SetWindowText("文件下载成功!");
						ReadWait = FALSE;
						WriteWait = FALSE;
						return 0;
					}else if (recvBuf[0] == 'A')
					{
						pSerialPort->m_editNote.SetWindowText("文件长度大于设备存储器!");
						pSerialPort->GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
						ReadWait = FALSE;
						WriteWait = FALSE;
						return 0;
					}else if (recvBuf[0] == 'E')
					{
						pSerialPort->m_editNote.SetWindowText("目标文件选择错误，请检查!");
						pSerialPort->GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
						ReadWait = FALSE;
						WriteWait = FALSE;
						return 0;
					}else if (recvBuf[0] == 'G')
					{
						pSerialPort->m_editNote.SetWindowText("文件下载失败,请重新下载!");
						pSerialPort->GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
						ReadWait = FALSE;
						WriteWait = FALSE;
						return 0;
					}else if (recvBuf[0] == 'O')
					{
						pSerialPort->m_editNote.SetWindowText("进入固件下载流程!");
						pSerialPort->GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
						ReadWait = FALSE;
						WriteWait = FALSE;
						return 0;
					}
					m_YModem.Recvchar=TRUE;
					pSerialPort->OnCommRecv(recvBuf, dwLength);
					m_YModem.Recvchar=FALSE;
					ZeroMemory(recvBuf, 1);
				}else
				{
					continue;
				}
			}
		}
	}		
	return 0;
}

DWORD xyrequest(LPVOID lpParameter)
{
	while(WriteWait)
	{  
		m_YModem.Modem_Request();	
	}
	return 0;
}

BOOL CDownLoadDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	GetCom();
	if (!InitCom())
	{
		GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow(FALSE);
	}
	m_YModem.Init_Modem(MODEM_SEND, (DWORD)this);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

/*  
*********************************************************************************************************
** 函数名称 :   OnCommRecv(DWORD UserParam, BYTE *buf, DWORD buflen)
** 函数功能 ：串口接收字符回调函数，用来接收来自设备的数据，并进行判断
** 入口参数 ：UserParam：用户指针
                               buf：    串口接收字符
							   buflen：  串口接收字符个数
** 出口参数 ：无
*********************************************************************************************************
*/
void CALLBACK CDownLoadDlg::OnCommRecv(BYTE *buf, DWORD buflen)
{  
     CString  WT[50];

	if(m_YModem.Recvchar==TRUE)
	{	
		m_YModem.Receivedata(buf[0]);
		Sleep(1);	   
	}		   
}

BOOL CDownLoadDlg::Senddata(BYTE *psendbuf, DWORD length) 
{
	BOOL bRet = TRUE;
	DWORD dwError;
	DWORD dwRealSend = 0;
	
	if (m_hCom == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	if (ClearCommError(m_hCom, &dwError, NULL))
	{
		PurgeComm(m_hCom, PURGE_TXABORT | PURGE_TXCLEAR);
	}
	
	OVERLAPPED olWrite;
	memset(&olWrite,0,sizeof(olWrite));
	olWrite.hEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
	
	m_criticalSection.Lock();
	bRet = WriteFile(m_hCom, psendbuf, length, &dwRealSend, &olWrite);	                  //从串口发送数据 
	m_criticalSection.Unlock();	
	GetOverlappedResult(m_hCom, &olWrite, &dwRealSend,TRUE);
	CloseHandle(olWrite.hEvent);
	
	return TRUE;
}

BOOL CDownLoadDlg::InitCom()
{
	//从注册表获取要打开的串口
	CString strsel;
	TCHAR buf[60] = {0};

	((CComboBox*)GetDlgItem(IDC_COMBO1))->GetWindowText(strsel);
	memcpy(buf, strsel.GetBuffer(strsel.GetLength()), strsel.GetLength());
	//通过制定的串口名称打开串口
	if(!ComConfig(buf))
	{
		return FALSE;
	}
	CHAR szEnter[] = "!@#$";
	DWORD dwRet = 0;
	DWORD dwLength = 0;
	BOOL fWriteState;
	OVERLAPPED olWrite;

	fWriteState = WriteFile(m_hCom,szEnter,strlen(szEnter),&dwRet,&olWrite);
	if (!fWriteState)
	{
		if (GetLastError() == ERROR_INVALID_HANDLE)
		{
			GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("没有检测到端口！");
			GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
			return FALSE;
		}
		else if (GetLastError() == ERROR_IO_PENDING)
			GetOverlappedResult(m_hCom, &olWrite, &dwLength, TRUE);
	}
	GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("进入固件下载流程!");

	return TRUE;
}

BOOL CDownLoadDlg::ComConfig(LPCTSTR lpComName)
{
	DWORD dwErr;
	CString strErr;
	char pName[32];
	memset(pName,0,32);
	memcpy(pName,"\\\\.\\",4);
	memcpy(pName+4,lpComName,strlen(lpComName));
	//打开串口
	m_hCom = CreateFile(pName,GENERIC_READ | GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,NULL);// 
	if(m_hCom == INVALID_HANDLE_VALUE)
	{
		dwErr = GetLastError();
		strErr.Format("%d,CreateFile",dwErr);
		//		AfxMessageBox(strErr);
		m_hCom = NULL;
		return FALSE;
	}
	//设置串口通行参数(配置BCD结构体)
	DCB myDCB;
	GetCommState(m_hCom,&myDCB);
	myDCB.BaudRate = CBR_57600; //CBR_57600  CBR_9600
	myDCB.fBinary = FALSE;
	myDCB.fParity = FALSE;
	myDCB.ByteSize = 8;
	myDCB.Parity = NOPARITY;
	myDCB.StopBits = ONESTOPBIT;
	SetCommState(m_hCom,&myDCB);  //设置串口通信参数
	//设置I/O缓冲区大小
	if(!SetupComm(m_hCom,8192,1024))   //4096
	{
		dwErr = GetLastError();
		strErr.Format("%d,SetupComm",dwErr);
		AfxMessageBox(strErr);
		return FALSE;
	}
	
	//设置超时
	COMMTIMEOUTS TimeOuts;
	//读超时  倍数*字节 + 常量]
	TimeOuts.ReadIntervalTimeout=500; 
	TimeOuts.ReadTotalTimeoutMultiplier=5; //5
	TimeOuts.ReadTotalTimeoutConstant=200; //200
	//写超时 
	TimeOuts.WriteTotalTimeoutMultiplier=1; 
	TimeOuts.WriteTotalTimeoutConstant=200; 
	SetCommTimeouts(m_hCom,&TimeOuts);  //设置超时 
	
	PurgeComm(m_hCom,PURGE_TXCLEAR | PURGE_RXCLEAR);  //清空缓冲区
	return TRUE;
}
void CDownLoadDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDownLoadDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDownLoadDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

char fileBuff[1024 * 1024];
void CDownLoadDlg::OnButtonDown() 
{
	// TODO: Add your control notification handler code here
	DWORD dwErr,dwRet = 0;
	CString strErr;
	DWORD dwLength = 0;
	DWORD dwError;
	CHAR szStart[] = "update\r\n";
	CHAR szRec[5] = {0};
	CHAR flag = 0;
	GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("");
	BOOL fReadState;
	BOOL fWriteState;
	OVERLAPPED olRead;
	OVERLAPPED olWrite;

	memset(&olWrite,0,sizeof(olWrite));
	memset(&olRead,0,sizeof(olRead)); 
	olRead.hEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
	olWrite.hEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
	if (ClearCommError(m_hCom, &dwError, NULL))
	{
		PurgeComm(m_hCom, PURGE_TXABORT | PURGE_TXCLEAR);
	}
	CFile file;	
	if(!file.Open(this->m_strPath.LockBuffer(),CFile::modeRead))
	{
		GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("请选择烧录文件!");
		GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
		return;
	}
	fReadState = ReadFile(m_hCom,szRec,sizeof(szRec),&dwRet,&olRead);
	if (!fReadState)
	{
		if (GetLastError() == ERROR_INVALID_HANDLE)
		{
			GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("没有检测到端口！");
			GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
			return;
		}
		else if (GetLastError() == ERROR_IO_PENDING)
			GetOverlappedResult(m_hCom, &olRead, &dwLength, TRUE);
	}
	if (szRec[0] == 'C')
	{
		flag = 1;
		GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("准备升级程序,请稍等...");
	}
	if (flag == 0)
	{
		fWriteState = WriteFile(m_hCom,szStart,strlen(szStart),&dwRet,&olWrite);
		if (!fWriteState)
		{
			if (GetLastError() == ERROR_INVALID_HANDLE)
			{
				GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("没有检测到端口！");
				GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
				return;
			}
			else if (GetLastError() == ERROR_IO_PENDING)
				GetOverlappedResult(m_hCom, &olWrite, &dwLength, TRUE);
		}
		GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("发送升级命令成功,准备升级程序...");
		Sleep(1000);
	}
	unsigned long fileLength=file.GetLength();
	if(file.Read(fileBuff,fileLength)<1)
	{
		file.Close();
		GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
		return;
	}
	else
	{
		fileBuff[fileLength]='\0';
	}
	CHAR*  sz=(char*)this->m_strPath.GetBuffer(this->m_strPath.GetLength()); 
	m_YModem.SendFile(sz, fileLength);
	m_YModem.Receivebuf(fileBuff,fileLength);
	//	delete[] fileBuff;
	this->m_strPath.ReleaseBuffer();
	ReadWait = TRUE;
	WriteWait = TRUE;
	m_WriteThread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)xyrequest,this,0,NULL);
	if(m_WriteThread == (HANDLE)-1){
		dwErr = GetLastError();
		strErr.Format("烧录线程创建失败，错误码为%d",dwErr);
		AfxMessageBox(strErr);
		return;
	}
	m_ReadThread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)CommRecvTread,this,0,NULL);
	if(m_ReadThread == (HANDLE)-1){
		dwErr = GetLastError();
		strErr.Format("烧录线程创建失败，错误码为%d",dwErr);
		AfxMessageBox(strErr);
		return;
	}
	CloseHandle(m_WriteThread);
	CloseHandle(m_ReadThread);
}

void CDownLoadDlg::OnButtonChoice() 
{
	// TODO: Add your control notification handler code here
	CFileDialog dlg(TRUE,NULL,NULL ,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT ,"(*.bin)|*.bin||");
	dlg.m_ofn.lpstrTitle = "打开 S-记录 文件";
	CString str;
	if(dlg.DoModal()==IDOK){
		//str = dlg.GetPathName();
		str = dlg.GetPathName();
		m_strPath = str;
		GetDlgItem(IDC_EDIT_FILEINFO)->SetWindowText(m_strPath);
		GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("");
	}
}

void CDownLoadDlg::OnBtnCancel() 
{
	// TODO: Add your control notification handler code here
	ReadWait = FALSE;
	WriteWait = FALSE;
	OnCancel();
}

void CDownLoadDlg::OnButtonReconn() 
{	
	CString strsel;
	TCHAR buf[60] = {0};
	CHAR szEnter[] = "!@#$";
	DWORD dwRet = 0;
	DWORD dwLength = 0;
	BOOL fWriteState;
	OVERLAPPED olWrite;

	((CComboBox*)GetDlgItem(IDC_COMBO1))->GetWindowText(strsel);
	memcpy(buf, strsel.GetBuffer(strsel.GetLength()), strsel.GetLength());
	//通过制定的串口名称打开串口
	if(!ComConfig(buf))
	{
		return;
	}
	
	fWriteState = WriteFile(m_hCom,szEnter,strlen(szEnter),&dwRet,&olWrite);
	if (!fWriteState)
	{
		if (GetLastError() == ERROR_INVALID_HANDLE)
		{
			GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("没有检测到端口！");
			GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
			return;
		}
		else if (GetLastError() == ERROR_IO_PENDING)
			GetOverlappedResult(m_hCom, &olWrite, &dwLength, TRUE);
	}
	GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("进入固件下载流程!");
}

void CDownLoadDlg::GetCom()
{
	//程序启动时获取全部可用串口
	HANDLE  hCom;
	int     i,k;
	CString str;

	((CComboBox *)GetDlgItem(IDC_COMBO1))->ResetContent();  //清除框内的数据
	for(i=1;i<=16;i++)
	{//此程序支持16个串口
		str.Format("\\\\.\\COM%d", i);    //使用俩个"\\"就是为了输出后面的"\\.\" ，单个"\"表示转义功能
										//假设i的值为1 ，则格式化后的结果为\\.\COM1
		hCom = CreateFile(str,0,0,0,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
					//CreatFile函数创建或打开一个对象，并返回一个可以用于访问此对象的句柄
		            //指向文件名的指针，访问模式(写/读)，共享模式，指向安全属性的指针，如何创建，文件属性，用于复制文件的句柄
		if(INVALID_HANDLE_VALUE != hCom)  //INVALID_HANDLE_VALUE被微软定义为-1的，在此表示无效的返回值
		{//能打开该串口，则添加该串口
			CloseHandle(hCom);
			str =str.Mid(4);     //截取出字符串，从第4位开始截取，即从\\.\COMn 中的第4位C开始截取，直到字符串结束
			((CComboBox *)GetDlgItem(IDC_COMBO1))->AddString(str);  //添加字符串
		}
	}

	i = ((CComboBox *)GetDlgItem(IDC_COMBO1))->GetCount();
	if(i == 0)
	{//若找不到可用串口则禁用“打开串口”功能
		((CComboBox *)GetDlgItem(IDC_COMBO1))->EnableWindow(false);
	}
	else
	{
		k = ((CComboBox *)GetDlgItem(IDC_COMBO1))->GetCount();
		((CComboBox *)GetDlgItem(IDC_COMBO1))->SetCurSel(k-1);
	}
}

void CDownLoadDlg::OnSelchangeCombo1() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	//CloseHandle(m_hCom);
	CString strsel;
	TCHAR buf[60] = {0};
	CHAR szEnter[] = "!@#$";
	DWORD dwRet = 0;
	DWORD dwLength = 0;
	BOOL fWriteState;
	OVERLAPPED olWrite;
	
	int cursel = ((CComboBox *)GetDlgItem(IDC_COMBO1))->GetCurSel();
	((CComboBox*)GetDlgItem(IDC_COMBO1))->GetWindowText(strsel);
	memcpy(buf, strsel.GetBuffer(strsel.GetLength()), strsel.GetLength());
	//通过制定的串口名称打开串口
	if(!ComConfig(buf))
	{
		return;
	}

	fWriteState = WriteFile(m_hCom,szEnter,strlen(szEnter),&dwRet,&olWrite);
	if (!fWriteState)
	{
		if (GetLastError() == ERROR_INVALID_HANDLE)
		{
			GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("没有检测到端口！");
			GetDlgItem(IDC_BUTTON_DOWN)->EnableWindow();
			return;
		}
		else if (GetLastError() == ERROR_IO_PENDING)
			GetOverlappedResult(m_hCom, &olWrite, &dwLength, TRUE);
	}
	GetDlgItem(IDC_EDIT_NOTE)->SetWindowText("进入固件下载流程!");

	UpdateData(false);
}
