// DownLoadDlg.h : header file
//

#if !defined(AFX_DOWNLOADDLG_H__1857EBA8_4AE3_46DC_9A2F_E6B1517D9362__INCLUDED_)
#define AFX_DOWNLOADDLG_H__1857EBA8_4AE3_46DC_9A2F_E6B1517D9362__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CDownLoadDlg dialog

class CDownLoadDlg : public CDialog
{
// Construction
public:
	CDownLoadDlg(CWnd* pParent = NULL);	// standard constructor
	HANDLE m_hCom;
	CString m_strPath;
	HANDLE m_WriteThread;	//�߳̾��
	HANDLE m_ReadThread;	//�߳̾��
	BOOL InitCom();
	void GetCom();
	BOOL ComConfig(LPCTSTR lpComName);
	BOOL Senddata(BYTE *psendbuf, DWORD length);
	void CALLBACK OnCommRecv(BYTE *buf, DWORD buflen);
// Dialog Data
	//{{AFX_DATA(CDownLoadDlg)
	enum { IDD = IDD_DOWNLOAD_DIALOG };
	CEdit	m_editProgress;
	CEdit	m_editNote;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDownLoadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDownLoadDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonDown();
	afx_msg void OnButtonChoice();
	afx_msg void OnBtnCancel();
	afx_msg void OnButtonReconn();
	afx_msg void OnSelchangeCombo1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOWNLOADDLG_H__1857EBA8_4AE3_46DC_9A2F_E6B1517D9362__INCLUDED_)
