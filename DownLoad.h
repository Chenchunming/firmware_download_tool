// DownLoad.h : main header file for the DOWNLOAD application
//

#if !defined(AFX_DOWNLOAD_H__05880F6A_CFBC_49F9_A84F_5FA4B7A7D04F__INCLUDED_)
#define AFX_DOWNLOAD_H__05880F6A_CFBC_49F9_A84F_5FA4B7A7D04F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDownLoadApp:
// See DownLoad.cpp for the implementation of this class
//

class CDownLoadApp : public CWinApp
{
public:
	CDownLoadApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDownLoadApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDownLoadApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOWNLOAD_H__05880F6A_CFBC_49F9_A84F_5FA4B7A7D04F__INCLUDED_)
